﻿// Copyright(c) 2018, 2019 Axel Gembe <derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using YamlDotNet.Serialization;

namespace ListingMaker
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow()
        {
            DataContext = this;

            InitializeComponent();

            Clear_Click(null, null);

            //Listing.Url = "https://www.hosting.co.uk/";
            //Listing.NameShort = "hostingcouk";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Listing_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ListingYAML"));

            if ("Name".Equals(e.PropertyName)) {
                MakeShortName();
            }
        }

        private void MakeShortName()
        {
            if (!string.IsNullOrWhiteSpace(Listing.Name)) {
                var matches = shortNameRegex_.Matches(Listing.Name.ToLower()).Cast<Match>();

                var shortname = string.Join("", matches.Select(m => m.Value).ToArray());
                shortname = shortname.Substring(0, Math.Min(15, shortname.Length));

                Listing.NameShort = shortname;
            } else {
                Listing.NameShort = string.Empty;
            }
        }

        private IconMaker iconMaker_ = new IconMaker();

        public Listing Listing { get; private set; } = new Listing();

        public string ListingYAML
        {
            get {
                var res = yamlSerializer_.Serialize(new Listing[] { Listing });
                return res;
            }
        }

        private ISerializer yamlSerializer_ = new SerializerBuilder().Build();

        public ObservableCollection<string> ListingIconCandidates { get; private set; } = new ObservableCollection<string>();

        public Region[] Regions { get; private set; } = Region.List;

        public ISO3166.Country[] Countries { get; private set; } = ISO3166.Country.List;

        private void SetButtonsIsEnabled(bool enabled)
        {
            DetectButton.IsEnabled = enabled;
            MakeButton.IsEnabled = enabled;
            ClearButton.IsEnabled = enabled;
        }

        private static readonly Regex twitterLinkRegex_ = new Regex("^https?://(?:.*\\.)?twitter\\.com/([^/]*)");
        private static readonly Regex facebookLinkRegex_ = new Regex("^https?://(?:.*\\.)?facebook\\.com/([^/]*)");
        private static readonly Regex emailLinkRegex_ = new Regex("^mailto:(.*)");
        private static readonly Regex shortNameRegex_ = new Regex("\\w*");

        private async void Detect_Click(object sender, RoutedEventArgs e)
        {
            SetButtonsIsEnabled(false);

            try {
                var html = await Utilities.DownloadAsync(new Uri(Listing.Url));

                var doc = new HtmlDocument();
                doc.LoadHtml(html);

                Listing.Name = doc.DocumentNode.SelectNodes("//title")?.FirstOrDefault()?.InnerText;

                var links = doc.DocumentNode.SelectNodes("//a")?
                    .Where(l => l.Attributes.Contains("href"))
                    .Select(l => l.Attributes["href"].Value)
                    .ToList();

                Listing.Twitter = links?.Select(l => twitterLinkRegex_.Match(l)).Where(m => m.Success).FirstOrDefault()?.Groups[1].Value;
                Listing.Facebook = links?.Select(l => facebookLinkRegex_.Match(l)).Where(m => m.Success).FirstOrDefault()?.Groups[1].Value;
                Listing.Email = links?.Select(l => emailLinkRegex_.Match(l)).Where(m => m.Success).FirstOrDefault()?.Groups[1].Value;
            } catch (WebException ex) {
                MessageBox.Show(this, $"{ex.Message}\n{ex.StackTrace}", Title, MessageBoxButton.OK, MessageBoxImage.Error);
            } finally {
                SetButtonsIsEnabled(true);
            }
        }

        private async void Make_Click(object sender, RoutedEventArgs e)
        {
            SetButtonsIsEnabled(false);

            try {
                ListingIconCandidates.Clear();

                var args = new Dictionary<string, string>();

                if (!string.IsNullOrWhiteSpace(Listing.Facebook))
                    args["facebook"] = Listing.Facebook;

                if (!string.IsNullOrWhiteSpace(Listing.Twitter))
                    args["twitter"] = Listing.Twitter;

                foreach (var c in await iconMaker_.Make(Listing.NameShort, Listing.Url, args))
                    ListingIconCandidates.Add(c);
            } finally {
                SetButtonsIsEnabled(true);
            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            Listing = new Listing();
            Listing.PropertyChanged += Listing_PropertyChanged;
            ListingIconCandidates.Clear();
        }

        private void CtrlCCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var listBox = sender as ListBox;

            var selectedItems = listBox.SelectedItems.Cast<string>();
            if (selectedItems.Count() == 0)
                return;

            CopyImageFile(selectedItems.First());
        }

        private void RightClickCopyCmdExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            if (menuItem.DataContext is string selectedItem)
                CopyImageFile(selectedItem);
        }

        private void CopyImageFile(string fileName)
        {
            var targetFileName = Path.Combine(iconMaker_.OutputDirectory, Listing.Image);

            File.Copy(fileName, targetFileName, true);

            Clipboard.SetFileDropList(new StringCollection() { targetFileName });
        }

        private void CopyCmdCanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;

        private void ResetRegion_Click(object sender, RoutedEventArgs e)
        {
            Listing.Region = null;
        }

        private void ResetCountry_Click(object sender, RoutedEventArgs e)
        {
            Listing.Country = null;
        }
    }
}
