﻿// Copyright(c) 2018, 2019 Axel Gembe <derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace ListingMaker.Converters
{
    public class PathToImageInfoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
                throw new NotSupportedException();

            var stringValue = value as string;

            var fileInfo = new FileInfo(stringValue);

            return $"File size: {fileInfo.Length} bytes";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}
