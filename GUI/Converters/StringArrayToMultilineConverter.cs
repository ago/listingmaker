﻿// Copyright(c) 2018, 2019 Axel Gembe <derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using System;
using System.Globalization;
using System.Windows.Data;

namespace ListingMaker.Converters
{
    public class StringArrayToMultilineConverter : IValueConverter
    {
        private static readonly char[] _separators = new char[] { '\r', '\n' };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var array = value as string[];
            if (array != null)
                return string.Join("\n", array);

            var str = value as string;
            return str?.Split(_separators, StringSplitOptions.RemoveEmptyEntries);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => Convert(value, targetType, parameter, culture);
    }
}
